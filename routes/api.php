<?php

$k = 'k';

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('user')->group(function () {

    Route::post('login',                                'UserController@login');
    Route::post('forgot',                               'UserController@forgot_password');

    Route::group(['middleware' => ['jwt.verify']], function () {

        Route::get('logout',                            'UserController@logout');
        Route::put('change',                            'UserController@change_password');
        Route::put('update',                            'UserController@update_user');
        Route::put('location',                          'UserController@update_location');

    });

    Route::group(['middleware' => ['jwt.manager']], function () {
        Route::get('get',                               'UserController@get_user_list');
    });

    Route::group(['middleware' => ['jwt.admin']], function () {

        Route::post('register',                         'UserController@register');
        Route::get('get/{_id}',                         'UserController@get_user_byId');
        Route::post('approve-reset-password/{_id}',     'UserController@approve_reset_password');

    });

});

Route::prefix('admin')->group(function () {

    Route::group(['middleware' => ['jwt.admin']], function () {

        Route::prefix('company')->group(function () {

            Route::post('add',                        'AdminController@add_company');
            Route::put('edit/{_id}',                  'AdminController@edit_company');
            Route::delete('delete/{_id}',             'AdminController@delete_company');

        });

        Route::prefix('area')->group(function () {

            Route::post('add',                         'AdminController@add_area');
            Route::put('edit/{_id}',                   'AdminController@edit_area');
            Route::delete('delete/{_id}',              'AdminController@delete_area');

        });

    });

    Route::group(['middleware' => ['jwt.verify']], function () {

        Route::get('area',                             'AdminController@get_area');
        Route::get('area/{_id}',                       'AdminController@get_area_byId');
        Route::get('area/company/{_id}',               'AdminController@get_area_byCompany');
        Route::get('company',                          'AdminController@get_company');
        Route::get('company/{_id}',                    'AdminController@get_company_byId');

    });

});

Route::prefix('customer')->group(function () {

    Route::group(['middleware' => ['jwt.manager']], function () {

        Route::post('add',                             'CustomerController@add_customer');
        Route::put('edit/{_id}',                       'CustomerController@edit_customer');
        Route::delete('delete/{_id}',                  'CustomerController@delete_customer');

    });

    Route::group(['middleware' => ['jwt.verify']], function () {

        Route::get('get',                              'CustomerController@get_customer');
        Route::get('get_by',                           'CustomerController@get_customer_byCondition');
        Route::get('get_array',                        'CustomerController@get_customer_byArray');
        Route::get('get/{_id}',                        'CustomerController@get_customer_byId');

    });

});

Route::prefix('route')->group(function () {

    Route::group(['middleware' => ['jwt.verify']], function () {

        Route::post('add',                             'RouteController@create_route');
        Route::put('edit/{_id}',                       'RouteController@edit_route');
        Route::delete('delete/{_id}',                  'RouteController@delete_route');
        Route::get('get',                              'RouteController@get_route');
        Route::get('get_by',                           'RouteController@get_route_byCondition');
        Route::get('get/{_id}',                        'RouteController@get_route_byId');
        Route::post('update-expense',                  'RouteController@update_expense');
        Route::post('update-customer',                 'RouteController@update_customer');
        Route::post('update-checklist',                'RouteController@update_checklist');
        Route::post('update-checklist-route',          'RouteController@update_checklist_route');
        Route::post('delete-customer-expense',         'RouteController@delete_customer_expense');
        Route::post('delete-route-expense',            'RouteController@delete_route_expense');
        Route::post('delete-route-customer',           'RouteController@delete_route_customer');
    });

    Route::group(['middleware' => ['jwt.manager']], function () {

        Route::put('approve/{_id}',                    'RouteController@approve_route');
        Route::put('assign',                           'RouteController@assigned_route');

    });

});



