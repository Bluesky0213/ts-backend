<?php
/**
 * Created by PhpStorm.
 * User: BlueSky
 * Date: 2018-09-19
 * Time: 06:08 PM
 */

namespace App\Http\Models;


use App\Http\Models\Schema\Location;
use Suren\LaravelMongoModelSchema\MongoModel;

class Company extends MongoModel
{

    protected $connection = 'mongodb';
    protected $collection = 'Company';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'contact', 'phone', 'address', 'location', 'service', 'product'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at'
    ];

    /**
     * The collection schema
     */
    public static function SCHEMAS()
    {
        return [
            'name'      => ['type' => 'string'],
            'contact'   => ['type' => 'string', 'default' => ''],
            'phone'     => ['type' => 'string', 'default' => ''],
            'address'   => ['type' => 'string', 'default' => ''],
            'location'  => ['type' => Location::class, 'default' => new Location()],
            'service'   => ['type' => 'array(string)', 'default' => []],
            'product'   => ['type' => 'array(string)', 'default' => []],
        ];
    }

}