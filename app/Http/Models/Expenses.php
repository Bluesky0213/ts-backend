<?php
/**
 * Created by PhpStorm.
 * User: BlueSky
 * Date: 2018-09-19
 * Time: 06:08 PM
 */

namespace App\Http\Models;


use App\Http\Models\Schema\Location;
use Suren\LaravelMongoModelSchema\MongoModel;

class Expenses extends MongoModel
{

    protected $connection = 'mongodb';
    protected $collection = 'Expenses';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date', 'time', 'title', 'amount', 'details', 'billable', 'attachment'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at'
    ];

    /**
     * The collection schema
     */
    public static function SCHEMAS()
    {
        return [
            'date'    => ['type' => 'string'],
            'time'    => ['type' => 'string', 'default' => ''],
            'title'   => ['type' => 'string', 'default' => ''],
            'amount'  => ['type' => 'float', 'default' => 0.0],
            'details'   => ['type' => 'string', 'default' => ''],
            'billable'   => ['type' => 'bool', 'default' => ''],
            'attachment'   => ['type' => 'string', 'default' => ''],
        ];
    }

}