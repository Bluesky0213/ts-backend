<?php
/**
 * Created by PhpStorm.
 * User: BlueSky
 * Date: 2018-09-22
 * Time: 12:04 AM
 */

namespace App\Http\Models\Schema;


use Suren\LaravelMongoModelSchema\NestedMongoModel;

class CheckPoint extends NestedMongoModel
{

    protected $fillable = ['name', 'detail', 'is_checked'];

    public static function SCHEMAS()
    {
        return [
            'name'       => ['type' => 'string',  'default' => ''],
            'detail'     => ['type' => 'array(string)',  'default' => []],
            'is_checked' => ['type' => 'array(bool)', 'default' => []]
        ];
    }

}