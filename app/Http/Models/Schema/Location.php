<?php
/**
 * Created by PhpStorm.
 * User: BlueSky
 * Date: 2018-09-15
 * Time: 01:09 AM
 */

namespace App\Http\Models\Schema;

use Suren\LaravelMongoModelSchema\NestedMongoModel;

class Location extends NestedMongoModel
{

    protected $fillable = ['latitude', 'longitude'];

    public static function SCHEMAS()
    {
        return [
            'latitude'      => ['type' => 'float',  'default' => 0.0],
            'longitude'     => ['type' => 'float',  'default' => 0.0]
        ];
    }

}