<?php
/**
 * Created by PhpStorm.
 * User: BlueSky
 * Date: 2018-09-21
 * Time: 11:59 PM
 */

namespace App\Http\Models\Schema;

use Suren\LaravelMongoModelSchema\NestedMongoModel;

class CheckList extends NestedMongoModel
{

    protected $fillable = ['customer_id', 'datetime', 'location', 'purpose', 'visit_person', 'expenses', 'checklist'];

    public static function SCHEMAS()
    {
        return [
            'customer_id'   => ['type' => 'string'],
            'datetime'      => ['type' => 'string'],
            'location'      => ['type' => Location::class, 'default' => new Location()],
            'purpose'       => ['type' => 'string'],
            'visit_person'  => ['type' => 'string'],
            'expenses'      => ['type' => 'array(string)', 'default' => []],
            'checklist'     => ['type' => 'array(' . CheckPoint::class . ')', 'default' => []]
        ];
    }

}