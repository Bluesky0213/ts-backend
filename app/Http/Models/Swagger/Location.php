<?php
/**
 * Created by PhpStorm.
 * User: BlueSky
 * Date: 2018-10-10
 * Time: 12:33 AM
 */

namespace App\Http\Models\Swagger;

/**
 * @OA\Schema(
 *   @OA\Xml(name="Location")
 * )
 */
class Location
{

    /**
     * @OA\Property(format="double")
     * @var number
     */
    public $latitude;

    /**
     * @OA\Property(format="double")
     * @var number
     */
    public $longitude;

}