<?php
/**
 * Created by PhpStorm.
 * User: BlueSky
 * Date: 2018-09-22
 * Time: 12:24 AM
 */

namespace App\Http\Models\Swagger;

/**
 * @OA\Schema(
 *   @OA\Xml(name="CheckPoint")
 * )
 */
class CheckPoint
{

    /**
     * @OA\Property()
     * @var string
     */
    public $name;

    /**
     * @OA\Property()
     * @var string[]
     */
    public $detail;

    /**
     * @OA\Property()
     * @var boolean[]
     */
    public $is_checked;

}