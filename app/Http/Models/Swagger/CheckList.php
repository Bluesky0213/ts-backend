<?php
/**
 * Created by PhpStorm.
 * User: BlueSky
 * Date: 2018-09-22
 * Time: 12:24 AM
 */

namespace App\Http\Models\Swagger;


/**
 * @OA\Schema(
 *   @OA\Xml(name="CheckList")
 * )
 */
class CheckList
{

    /**
     * @OA\Property()
     * @var string
     */
    public $customer_id;

    /**
     * @var \DateTime
     * @OA\Property()
     */
    public $datetime;

    /**
     * @OA\Property(format="double")
     * @var number
     */
    public $latitude;

    /**
     * @OA\Property(format="double")
     * @var number
     */
    public $longitude;

    /**
     * @OA\Property()
     * @var string
     */
    public $purpose;

    /**
     * @OA\Property()
     * @var string
     */
    public $visit_person;

    /**
     * @var string[]
     * @OA\Property()
     */
    public $expenses;

    /**
     * @var CheckPoint[]
     * @OA\Property(@OA\Xml(name="CheckPoint"))
     */
    public $checkpoints;



}