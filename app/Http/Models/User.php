<?php
/**
 * Created by PhpStorm.
 * User: BlueSky
 * Date: 2018-09-14
 * Time: 07:54 PM
 */

namespace App\Http\Models;

use App\Http\Models\Schema\Location;
use Illuminate\Support\Facades\Hash;
use Suren\LaravelMongoModelSchema\MongoModel;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends MongoModel implements JWTSubject
{

    protected $connection = 'mongodb';
    protected $collection = 'Users';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'phone', 'password', 'role', 'location', 'company_id', 'area_id', 'auth_token', 'timestamp'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'auth_token', 'timestamp' ,'updated_at'
    ];

    /**
     * The collection schema
     */
    public static function SCHEMAS()
    {
        return [
            'username'      => ['type' => 'string'],
            'email'         => ['type' => 'string'],
            'phone'         => ['type' => 'string'],
            'password'      => ['type' => 'string', 'default' => Hash::make("123456")],
            'role'          => ['type' => 'string', 'default' => 'USER'],
            'location'      => ['type' => Location::class, 'default' => new Location()],
            'company_id'    => ['type' => 'string', 'default' => ''],
            'area_id'       => ['type' => 'string', 'default' => ''],
            'auth_token'    => ['type' => 'string', 'default' => ''],
            'timestamp'     => ['type' => 'timestamp', 'default' => 0]
        ];
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }


    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function Company() {
        return $this->belongsTo(Company::class,'company_id','_id');
    }

    public function Area() {
        return $this->belongsTo(Area::class,'area_id','_id');
    }


}