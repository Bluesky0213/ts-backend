<?php

/**
 * Created by PhpStorm.
 * User: BlueSky
 * Date: 2018-09-21
 * Time: 11:54 PM
 */

namespace App\Http\Models;

use App\Http\Models\Schema\CheckPoint;
use App\Http\Models\Schema\CheckList;
use Suren\LaravelMongoModelSchema\MongoModel;


class Route extends MongoModel
{

    protected $connection = 'mongodb';
    protected $collection = 'Route';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'created_user', 'assigned_user', 'expenses', 'checklist', 'customer_list', 'is_approved'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       'updated_at'
    ];

    /**
     * The collection schema
     */
    public static function SCHEMAS()
    {
        return [
            'name'              => ['type' => 'string'],
            'created_user'      => ['type' => 'string'],
            'assigned_user'     => ['type' => 'string', 'default' => ''],
            'expenses'          => ['type' => 'array(string)', 'default' => []],
            'checklist'         => ['type' => 'array(' . CheckPoint::class . ')', 'default' => []],
            'customer_list'     => ['type' => 'array(' . CheckList::class . ')', 'default' => []],
            'is_approved'       => ['type' => 'bool', 'default' => false]
        ];
    }

    public function relation1() {
        return $this->belongsTo(User::class,'created_user','_id');
    }

    public function relation2() {
        return $this->belongsTo(User::class,'assigned_user','_id');
    }

}