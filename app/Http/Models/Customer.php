<?php
/**
 * Created by PhpStorm.
 * User: BlueSky
 * Date: 2018-09-20
 * Time: 02:57 AM
 */

namespace App\Http\Models;

use App\Http\Models\Schema\Location;
use Suren\LaravelMongoModelSchema\MongoModel;

class Customer extends MongoModel
{

    protected $connection = 'mongodb';
    protected $collection = 'Customer';


    /**
     *
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'phone', 'address', 'location', 'default_address', 'products', 'services', 'company_id', 'area_id'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at'
    ];


    /**
     * The collection schema
     */
    public static function SCHEMAS()
    {
        return [
            'name'          => ['type' => 'string'],
            'email'         => ['type' => 'string'],
            'phone'         => ['type' => 'string'],
            'address'       => ['type' => 'array(string)', 'default' => []],
            'location'      => ['type' => 'array(' . Location::class . ')', 'default' =>[]],
            'default_address' => ['type' => 'int', 'default' => 0],
            'products'      => ['type' => 'array(string)', 'default' => []],
            'services'      => ['type' => 'array(string)', 'default' => []],
            'company_id'    => ['type' => 'string', 'default' => ''],
            'area_id'       => ['type' => 'string', 'default' => '']
        ];
    }

    public function Company() {
        return $this->belongsTo(Company::class,'company_id','_id');
    }

    public function Area() {
        return $this->belongsTo(Area::class,'area_id','_id');
    }


}