<?php
/**
 * Created by PhpStorm.
 * User: BlueSky
 * Date: 2018-09-19
 * Time: 06:07 PM
 */

namespace App\Http\Models;

use Suren\LaravelMongoModelSchema\MongoModel;

class Area extends MongoModel
{

    protected $connection = 'mongodb';
    protected $collection = 'Areas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'company_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at'
    ];

    /**
     * The collection schema
     */
    public static function SCHEMAS()
    {
        return [
            'name'      => ['type' => 'string'],
            'company_id'    => ['type' => 'string', 'default' => '']
        ];
    }

    public function Company(){
        return $this->belongsTo(Company::class,'company_id','_id');
    }

}