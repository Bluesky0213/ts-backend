<?php

namespace App\Http\Middleware;

use App\Http\Models\User;
use Closure;
use Illuminate\Support\Facades\Log;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class AdminMiddleware extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $token = getallheaders()['auth_token'];

        if ($token == null || $token == '') {
            return response()->json(['status' => false, 'message' => "Authorization Token not found", 'data' => []], 403);
        }

        $user = User::where('auth_token', $token)->first();

        if ($user == null) {
            return response()->json(['status' => false, 'message' => "Authorization Token is invalid", 'data' => []], 403);
        }

        $timestamp = floor((time() - $user->timestamp) / 60);
        if ($timestamp > 60 * 24) {
            return response()->json(['status' => false, 'message' => "Authorization Token is expired", 'data' => []], 403);
        }

        if ($user->role != 'ADMIN') {
            return response()->json(['status' => false, 'message' => "User permission denied", 'data' => []], 403);
        }

        return $next($request);
    }
}
