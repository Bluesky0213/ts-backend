<?php

namespace App\Http\Controllers;

use App\Http\Models\Schema\Location;
use App\Http\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use MongoDB\BSON\ObjectId;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserController extends Controller
{

    /**
     * @OA\Post(path="/user/register",
     *   tags={"User Controller"},
     *   summary="Register new user",
     *   description="",
     *   operationId="createUser",
     *   @OA\Parameter(
     *     name="auth_token",
     *     required=true,
     *     in="header",
     *     description="The authorization token",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="username",
     *     required=true,
     *     in="query",
     *     description="The user name",
     *     @OA\Schema(
     *         type="string",
     *         minLength=6,
     *         maxLength=20
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="email",
     *     required=true,
     *     in="query",
     *     description="The user email",
     *     @OA\Schema(
     *         type="string",
     *         format="email"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="phone",
     *     required=true,
     *     in="query",
     *     description="The user phone number",
     *     @OA\Schema(
     *         type="string",
     *         minLength=10,
     *         maxLength=20,
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="role",
     *     required=true,
     *     in="query",
     *     description="The user role",
     *     @OA\Schema(
     *         type="string",
     *         enum={"USER", "MANAGER", "ADMIN"}
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="latitude",
     *     required=true,
     *     in="query",
     *     description="The current latitude of user",
     *     @OA\Schema(
     *         type="number",
     *         format="double"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="longitude",
     *     required=true,
     *     in="query",
     *     description="The current longitude of user",
     *     @OA\Schema(
     *         type="number",
     *         format="double"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="password",
     *     required=true,
     *     in="query",
     *     description="The user password",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="company_id",
     *     in="query",
     *     description="company id of user",
     *     @OA\Schema(
     *         type="Object Id",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="area_id",
     *     in="query",
     *     description="area id of user",
     *     @OA\Schema(
     *         type="Object Id",
     *     )
     *   ),
     *   @OA\Response(response="201", description="new user created successfully"),
     *   @OA\Response(response="400", description="parameter error"),
     *   @OA\Response(response="404", description="not found error"),
     *   @OA\Response(response="500", description="internal server error"),
     * )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request) {

        $validator = Validator::make($request->all(), [
            'username' => 'required|string',
            'email' => 'required|string|email',
            'phone' => 'required|string',
            'role' => 'required|string',
            'latitude' => 'required|numeric',
            'longitude' => 'required|numeric',
            'password' => 'required|string'
        ]);

        if($validator->fails()){
            return response()->json($this->configFailArray($validator->messages()), 400);
        }

        if(User::where('username', $request->get('username'))->first() != null) {
            return response()->json($this->configFailArray("same username already is exist."));
        }

        if(User::where('email', $request->get('email'))->first() != null) {
            return response()->json($this->configFailArray("same email already is exist."));
        }

        if(User::where('phone', $request->get('phone'))->first() != null) {
            return response()->json($this->configFailArray("same phone number already is exist."));
        }

        $company_id = $request->get('company_id');
        if ($company_id == null) $company_id = (string) new ObjectID();

        $area_id = $request->get('area_id');
        if ($area_id == null) $area_id = (string) new ObjectId();

        $user = User::create([
            'username' => $request->get('username'),
            'email' => $request->get('email'),
            'phone' => $request->get('phone'),
            'role' => $request->get('role'),
            'location' => new Location([
                'latitude' => $request->get('latitude'),
                'longitude' => $request->get('longitude')
            ]),
            'password' => Hash::make($request->get('password')),
            'company_id' => $company_id,
            'area_id' => $area_id
        ]);

        $data = [
            'username' => $user->username,
            'role' => $user->role
        ];

        $email = $user->email;

        /*
        Mail::send('email.register', $data, function ($message) use ($email) {
            $message->from(env("MAIL_USERNAME"));
            $message->to($email);
            $message->subject("Register User");
        });
        */


        return response()->json(
            $this->configSuccessArray(
                "new user have created successfully.",
                $user->with('company')->with('area')->find($user->id)),
            201);
    }

    /**
     *
     *  @OA\Post(path="/user/login",
     *   tags={"User Controller"},
     *   summary="Login user into the system",
     *   description="",
     *   operationId="loginUser",
     *   @OA\Parameter(
     *     name="username",
     *     required=true,
     *     in="query",
     *     description="The username",
     *     @OA\Schema(
     *         type="string",
     *         minLength=6,
     *         maxLength=20
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="password",
     *     required=true,
     *     in="query",
     *     description="The user password confirm",
     *     @OA\Schema(
     *         type="string",
     *         minLength=6,
     *         maxLength=20,
     *         format="password"
     *     )
     *   ),
     *   @OA\Response(response="200", description="logged in successfully"),
     *   @OA\Response(response="400", description="parameter error"),
     *   @OA\Response(response="404", description="not found error"),
     *   @OA\Response(response="500", description="internal server error"),
     * )
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {

        $credentials = $request->only('username', 'password');

        $rules = [
            'username' => 'required|string',
            'password' => 'required|string',
        ];

        $validator = Validator::make($credentials, $rules);

        if($validator->fails()) {
            return response()->json($this->configFailArray($validator->messages()), 400);
        }

        $user = User::where('username', $request->get('username'))->first();

        if ($user == null) {
            return response()->json($this->configFailArray("username or password in invalid."));
        }

        if(Hash::check($request->get('password'), $user->password)) {

            $token = JWTAuth::fromUser($user);
            $user->auth_token = $token;
            $user->timestamp = time();
            $user->save();

            $data = [
                'user' => $user->with('company')->with('area')->find($user->id),
                'token' => $token
            ];

            return response()->json($this->configSuccessArray("user logged in successfully.", $data));
        } else {
            return response()->json($this->configFailArray("username or password in invalid."));
        }


    }

    /**
     *
     * @OA\Get(path="/user/get",
     *   tags={"User Controller"},
     *   summary="Get all user list",
     *   description="",
     *   operationId="allUserList",
     *   @OA\Parameter(
     *     name="auth_token",
     *     required=true,
     *     in="header",
     *     description="The authorization token",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Response(response="200", description="get all user list successfully"),
     *   @OA\Response(response="400", description="parameter error"),
     *   @OA\Response(response="403", description="authorize error"),
     *   @OA\Response(response="404", description="not found error"),
     *   @OA\Response(response="500", description="internal server error"),
     * )
     *
     *
     * get all user list
     * if user is not admin, get permission denied error
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_user_list() {
        return response()->json($this->configSuccessArray("User list get successfully", User::all()));
    }

    /**
     *
     * @OA\Get(path="/user/get/{_id}",
     *   tags={"User Controller"},
     *   summary="Get user info by Id",
     *   description="",
     *   operationId="userInfo",
     *   @OA\Parameter(
     *     name="auth_token",
     *     required=true,
     *     in="header",
     *     description="The authorization token",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="_id",
     *     required=true,
     *     in="path",
     *     description="The Object Id of user that get user info",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Response(response="200", description="get user info by _id successfully"),
     *   @OA\Response(response="400", description="parameter error"),
     *   @OA\Response(response="403", description="authorize error"),
     *   @OA\Response(response="404", description="not found error"),
     *   @OA\Response(response="500", description="internal server error"),
     * )
     *
     *
     * @param $_id : Object Id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_user_byId($_id) {

        $user = User::find($_id);

        if($user == null) {
            return response()->json($this->configFailArray("User not exist."));
        }

        return response()->json(
            $this->configSuccessArray(
                "get user info successfully",
                $user->with('company')->with('area')->find($_id)
            )
        );
    }

    /**
     *
     * @OA\Get(path="/user/logout",
     *   tags={"User Controller"},
     *   summary="Logs out current logged in user session",
     *   description="",
     *   operationId="logoutUser",
     *   @OA\Parameter(
     *     name="auth_token",
     *     required=true,
     *     in="header",
     *     description="The authorization token",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Response(response="200", description="logout successfully"),
     *   @OA\Response(response="400", description="parameter error"),
     *   @OA\Response(response="403", description="authorize error"),
     *   @OA\Response(response="404", description="not found error"),
     *   @OA\Response(response="500", description="internal server error"),
     * )
     *
     *
     * Log out
     * Invalidate the token, so user cannot use it anymore
     * They have to re login to get a new token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout() {

        $user = User::where('auth_token', getallheaders()['auth_token'])->first();
        $user->auth_token = "";
        $user->timestamp = 0;
        $user->save();

        return response()->json($this->configSuccessArray("The user has successfully logged out."));
    }

    /**
     *
     * @OA\Post(path="/user/forgot",
     *   tags={"User Controller"},
     *   summary="Reset password when user forgot password",
     *   description="",
     *   operationId="forgotPassword",
     *   @OA\Parameter(
     *     name="email",
     *     required=true,
     *     in="query",
     *     description="The email of user for reset password",
     *     @OA\Schema(
     *         type="string",
     *         format="email"
     *     )
     *   ),
     *   @OA\Response(response="200", description="reset password successfully"),
     *   @OA\Response(response="400", description="parameter error"),
     *   @OA\Response(response="403", description="authorize error"),
     *   @OA\Response(response="404", description="not found error"),
     *   @OA\Response(response="500", description="internal server error"),
     * )
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function forgot_password(Request $request) {

        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email',
        ]);

        if($validator->fails()) {
            return response()->json($this->configFailArray($validator->messages()), 400);
        }

        $user = User::where('email', $request->get('email'))->first();

        if ($user == null) {
            return response()->json($this->configFailArray("User not exist."));
        }

        $user->is_reset = true;
        $user->save();

        return response()->json($this->configSuccessArray("Reset password success.\nYou must wait approve of admin."));
    }

    /**
     *
     * @OA\Post(path="/user/approve_reset/{_id}",
     *   tags={"User Controller"},
     *   summary="Admin approve reset password request of user",
     *   description="",
     *   operationId="approveReset",
     *   @OA\Parameter(
     *     name="auth_token",
     *     required=true,
     *     in="header",
     *     description="The authorization token",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="_id",
     *     required=true,
     *     in="path",
     *     description="The Object Id of user that get user info",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Response(response="200", description="reset password successfully"),
     *   @OA\Response(response="400", description="parameter error"),
     *   @OA\Response(response="403", description="authorize error"),
     *   @OA\Response(response="404", description="not found error"),
     *   @OA\Response(response="500", description="internal server error"),
     * )
     *
     *
     * @param $_id : Object Id
     * @return \Illuminate\Http\JsonResponse
     */
    public function approve_reset_password($_id) {

        $user = User::find($_id);

        if ($user == null) {
            return response()->json($this->configFailArray("User not exist."));
        }

        $user->is_reset = false;
        $user->password = Hash::make("123456");
        $user->save();

        $data = ['username' => $user->username];

        $email = $user->email;

        Mail::send('email.reset', $data, function ($message) use ($email) {
            $message->from(env("MAIL_USERNAME"));
            $message->to($email);
            $message->subject("Reset Password");
        });

        return response()->json($this->configSuccessArray("User password reset successfully"));
    }

    /**
     *
     * @OA\Put(path="/user/change",
     *   tags={"User Controller"},
     *   summary="Change password of user",
     *   description="",
     *   operationId="changePassword",
     *   @OA\Parameter(
     *     name="auth_token",
     *     required=true,
     *     in="header",
     *     description="The authorization token",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="password",
     *     required=true,
     *     in="query",
     *     description="The user old password",
     *     @OA\Schema(
     *         type="string",
     *         minLength=6,
     *         maxLength=20,
     *         format="password"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="password_confirmation",
     *     required=true,
     *     in="query",
     *     description="The user old password confirm",
     *     @OA\Schema(
     *         type="string",
     *         minLength=6,
     *         maxLength=20,
     *         format="password"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="new_password",
     *     required=true,
     *     in="query",
     *     description="The user new password",
     *     @OA\Schema(
     *         type="string",
     *         minLength=6,
     *         maxLength=20,
     *         format="password"
     *     )
     *   ),
     *   @OA\Response(response="200", description="change password successfully"),
     *   @OA\Response(response="400", description="parameter error"),
     *   @OA\Response(response="403", description="authorize error"),
     *   @OA\Response(response="404", description="not found error"),
     *   @OA\Response(response="500", description="internal server error"),
     * )
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function change_password(Request $request) {

        $validator = Validator::make($request->all(), [
            'password' => 'required|string|confirmed',
            'new_password' => 'required|string'
        ]);

        if($validator->fails()) {
            return response()->json($this->configFailArray($validator->messages()), 400);
        }

        $user = User::where('auth_token', getallheaders()['auth_token'])->first();

        if(Hash::check($request->get('password'), $user->password)) {

            $user->password = Hash::make($request->get('new_password'));
            $user->save();

            return response()->json($this->configSuccessArray("password have changed successfully"));
        } else {
            return response()->json($this->configFailArray($validator->messages()), 400);
        }

    }

    /**
     *
     * @OA\Put(path="/user/update",
     *   tags={"User Controller"},
     *   summary="Update user info",
     *   description="",
     *   operationId="updateUser",
     *   @OA\Parameter(
     *     name="auth_token",
     *     required=true,
     *     in="header",
     *     description="The authorization token",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="username",
     *     required=true,
     *     in="query",
     *     description="The user name",
     *     @OA\Schema(
     *         type="string",
     *         minLength=6,
     *         maxLength=20
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="email",
     *     required=true,
     *     in="query",
     *     description="The user email",
     *     @OA\Schema(
     *         type="string",
     *         format="email"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="phone",
     *     required=true,
     *     in="query",
     *     description="The user phone number",
     *     @OA\Schema(
     *         type="string",
     *         minLength=10,
     *         maxLength=20,
     *     )
     *   ),
     *   @OA\Response(response="200", description="update user info successfully"),
     *   @OA\Response(response="400", description="parameter error"),
     *   @OA\Response(response="403", description="authorize error"),
     *   @OA\Response(response="404", description="not found error"),
     *   @OA\Response(response="500", description="internal server error"),
     * )
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update_user(Request $request) {

        $validator = Validator::make($request->all(), [
            'username' => 'required|string',
            'email' => 'required|string|email',
            'phone' => 'required|string',
        ]);

        if($validator->fails()){
            return response()->json($this->configFailArray($validator->messages()), 400);
        }

        $user = User::where('auth_token', getallheaders()['auth_token'])->first();

        $check = User::where('username', $request->get('username'))->first();
        if ($check != null && $check->_id != $user->_id) {
            return response()->json($this->configFailArray("same username already exist."));
        }

        $check = User::where('email', $request->get('email'))->first();
        if ($check != null && $check->_id != $user->_id) {
            return response()->json($this->configFailArray("same email already exist."));
        }

        $check = User::where('phone', $request->get('phone'))->first();
        if ($check != null && $check->_id != $user->_id) {
            return response()->json($this->configFailArray("same phone number already exist."));
        }

        $user->username = $request->get('username');
        $user->email = $request->get('email');
        $user->phone = $request->get('phone');
        $user->save();

        return response()->json(
            $this->configSuccessArray(
                "user info update successfully",
                $user->with('company')->with('area')->find($user->_id)
            )
        );
    }

    /**
     *
     * @OA\Put(path="/user/location",
     *   tags={"User Controller"},
     *   summary="Change location of user",
     *   description="",
     *   operationId="changeLocation",
     *   @OA\Parameter(
     *     name="auth_token",
     *     required=true,
     *     in="header",
     *     description="The authorization token",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="latitude",
     *     required=true,
     *     in="query",
     *     description="The current latitude of user",
     *     @OA\Schema(
     *         type="number",
     *         format="double"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="longitude",
     *     required=true,
     *     in="query",
     *     description="The current longitude of user",
     *     @OA\Schema(
     *         type="number",
     *         format="double"
     *     )
     *   ),
     *   @OA\Response(response="200", description="update user location successfully"),
     *   @OA\Response(response="400", description="parameter error"),
     *   @OA\Response(response="403", description="authorize error"),
     *   @OA\Response(response="404", description="not found error"),
     *   @OA\Response(response="500", description="internal server error"),
     * )
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update_location(Request $request) {

        $validator = Validator::make($request->all(), [
            'latitude' => 'required|numeric',
            'longitude' => 'required|numeric'
        ]);

        if($validator->fails()){
            return response()->json($this->configFailArray($validator->messages()), 400);
        }

        $user = User::where('auth_token', getallheaders()['auth_token'])->first();
        $user->location = new Location([$request->get('latitude'), $request->get('longitude')]);
        $user->save();

        return response()->json(
            $this->configSuccessArray(
                "user location update successfully",
                $user->with('company')->with('area')->find($user->_id)
            )
        );
    }


}
