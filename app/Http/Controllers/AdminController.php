<?php

namespace App\Http\Controllers;

use App\Http\Models\Area;
use App\Http\Models\Company;
use App\Http\Models\User;
use App\Http\Models\Customer;
use App\Http\Models\Schema\Location;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{

    /**
     *
     * @OA\Post(path="/admin/company/add",
     *   tags={"Admin Controller"},
     *   summary="Add company",
     *   description="",
     *   operationId="addCompany",
     *   @OA\Parameter(
     *     name="auth_token",
     *     required=true,
     *     in="header",
     *     description="The authorization token",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="name",
     *     required=true,
     *     in="query",
     *     description="The company name",
     *     @OA\Schema(
     *         type="string",
     *         minLength=4,
     *         maxLength=255
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="contact",
     *     required=true,
     *     in="query",
     *     description="The company contact info(email, phone number etc.)",
     *     @OA\Schema(
     *         type="string",
     *         minLength=4,
     *         maxLength=255
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="address",
     *     required=true,
     *     in="query",
     *     description="The company address",
     *     @OA\Schema(
     *         type="string",
     *         minLength=4,
     *         maxLength=255
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="latitude",
     *     required=true,
     *     in="query",
     *     description="The current latitude of company",
     *     @OA\Schema(
     *         type="number",
     *         format="double"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="longitude",
     *     required=true,
     *     in="query",
     *     description="The current longitude of company",
     *     @OA\Schema(
     *         type="number",
     *         format="double"
     *     )
     *   ),
     *
     *   @OA\Response(response="201", description="add new company success"),
     *   @OA\Response(response="400", description="parameter error"),
     *   @OA\Response(response="403", description="authorize error"),
     *   @OA\Response(response="404", description="not found error"),
     *   @OA\Response(response="500", description="internal server error"),
     * )
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function add_company(Request $request) {

        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'address' => 'required|string',
            'products' => 'required|array',
            'services' => 'required|array',
            'latitude' => 'required|numeric',
            'longitude' => 'required|numeric'
        ]);

        if($validator->fails()){
            return response()->json($this->configFailArray($validator->messages()), 400);
        }

        if (Company::where('name', $request->get('name'))->first() != null) {
            return response()->json($this->configFailArray("same company name already exist."));
        }

        $company = Company::create([
            'name' => $request->get('name'),
            'contact' => $request->get('contact'),
            'phone' => $request->get('phone'),
            'address' => $request->get('address'),
            'location' => new Location([
                'latitude' => $request->get('latitude'),
                'longitude' => $request->get('longitude')
            ]),
            'product' => $request->get('products'),
            'service' => $request->get('services'),
        ]);

        return response()->json($this->configSuccessArray("new company have created successfully.", $company), 201);
    }

    /**
     *
     * @OA\Put(path="/admin/company/edit/{_id}",
     *   tags={"Admin Controller"},
     *   summary="Edit company",
     *   description="",
     *   operationId="editCompany",
     *   @OA\Parameter(
     *     name="auth_token",
     *     required=true,
     *     in="header",
     *     description="The authorization token",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="_id",
     *     required=true,
     *     in="path",
     *     description="The company Object Id",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="name",
     *     required=true,
     *     in="query",
     *     description="The company name",
     *     @OA\Schema(
     *         type="string",
     *         minLength=4,
     *         maxLength=255
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="contact",
     *     required=true,
     *     in="query",
     *     description="The company contact info(email, phone number etc.)",
     *     @OA\Schema(
     *         type="string",
     *         minLength=4,
     *         maxLength=255
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="address",
     *     required=true,
     *     in="query",
     *     description="The company address",
     *     @OA\Schema(
     *         type="string",
     *         minLength=4,
     *         maxLength=255
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="latitude",
     *     required=true,
     *     in="query",
     *     description="The current latitude of company",
     *     @OA\Schema(
     *         type="number",
     *         format="double"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="longitude",
     *     required=true,
     *     in="query",
     *     description="The current longitude of company",
     *     @OA\Schema(
     *         type="number",
     *         format="double"
     *     )
     *   ),
     *
     *   @OA\Response(response="200", description="edit company success"),
     *   @OA\Response(response="400", description="parameter error"),
     *   @OA\Response(response="403", description="authorize error"),
     *   @OA\Response(response="404", description="not found error"),
     *   @OA\Response(response="500", description="internal server error"),
     * )
     *
     * @param Request $request
     * @param $_id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit_company(Request $request, $_id) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:4|max:255',
            'address' => 'required|string|min:4|max:255',
            'latitude' => 'required|numeric',
            'longitude' => 'required|numeric'
        ]);

        if($validator->fails()){
            return response()->json($this->configFailArray($validator->messages()), 400);
        }

        $company = Company::where('name', $request->get('name'))->first();
        if ($company != null && $company->_id != $_id) {
            return response()->json($this->configFailArray("same company name already exist."));
        }

        $company = Company::find($_id);
        $company->name = $request->get('name');
        $company->contact = $request->get('contact');
        $company->phone = $request->get('phone');
        $company->address = $request->get('address');
        $company->location = new Location([
            'latitude' => $request->get('latitude'),
            'longitude' => $request->get('longitude')
        ]);

        $company->product = $request->get('products');
        $company->service = $request->get('services');
        $company->save();

        return response()->json($this->configSuccessArray("company info have updated successfully.", $company));
    }

    /**
     *
     * @OA\DELETE(path="/admin/company/delete/{_id}",
     *   tags={"Admin Controller"},
     *   summary="Delete company",
     *   description="",
     *   operationId="deleteCompany",
     *   @OA\Parameter(
     *     name="auth_token",
     *     required=true,
     *     in="header",
     *     description="The authorization token",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="_id",
     *     required=true,
     *     in="path",
     *     description="The company Object Id",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *
     *   @OA\Response(response="200", description="delete company success"),
     *   @OA\Response(response="400", description="parameter error"),
     *   @OA\Response(response="403", description="authorize error"),
     *   @OA\Response(response="404", description="not found error"),
     *   @OA\Response(response="500", description="internal server error"),
     * )
     *
     * @param $_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete_company($_id) {

		$data = User::where('company_id', $_id)->first();
		if ($data != null)
			return response()->json($this->configFailArray("This company is already in use."));
		
		$data = Customer::where('company_id', $_id)->first();
		if ($data != null)
			return response()->json($this->configFailArray("This company is already in use."));

        Company::destroy($_id);
        return response()->json($this->configSuccessArray("company have deleted successfully."));

    }

    /**
     *
     * @OA\Get(path="/admin/company",
     *   tags={"Admin Controller"},
     *   summary="Get all company list",
     *   description="",
     *   operationId="getCompany",
     *   @OA\Parameter(
     *     name="auth_token",
     *     required=true,
     *     in="header",
     *     description="The authorization token",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *
     *   @OA\Response(response="200", description="all company list get successfully"),
     *   @OA\Response(response="400", description="parameter error"),
     *   @OA\Response(response="403", description="authorize error"),
     *   @OA\Response(response="404", description="not found error"),
     *   @OA\Response(response="500", description="internal server error"),
     * )
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_company() {
        return response()->json($this->configSuccessArray("Company list get successfully", Company::all()));
    }

    /**
     *
     * @OA\Get(path="/admin/company/{_id}",
     *   tags={"Admin Controller"},
     *   summary="Get company info by Id",
     *   description="",
     *   operationId="companyInfo",
     *   @OA\Parameter(
     *     name="auth_token",
     *     required=true,
     *     in="header",
     *     description="The authorization token",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="_id",
     *     required=true,
     *     in="path",
     *     description="The Object Id of company that get user info",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Response(response="200", description="get company info by _id successfully"),
     *   @OA\Response(response="400", description="parameter error"),
     *   @OA\Response(response="403", description="authorize error"),
     *   @OA\Response(response="404", description="not found error"),
     *   @OA\Response(response="500", description="internal server error"),
     * )
     *
     *
     * @param $_id : Object Id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_company_byId($_id) {
        $company = Company::find($_id);

        if($company == null) {
            return response()->json($this->configFailArray("Company not exist."));
        }

        return response()->json($this->configSuccessArray("get company info successfully", $company));

    }

    /**
     *
     * @OA\Post(path="/admin/area/add",
     *   tags={"Admin Controller"},
     *   summary="Add area",
     *   description="",
     *   operationId="addArea",
     *   @OA\Parameter(
     *     name="auth_token",
     *     required=true,
     *     in="header",
     *     description="The authorization token",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="name",
     *     required=true,
     *     in="query",
     *     description="The area name",
     *     @OA\Schema(
     *         type="string",
     *         minLength=4,
     *         maxLength=255
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="company_id",
     *     in="query",
     *     description="company id of area",
     *     @OA\Schema(
     *         type="Object Id",
     *     )
     *   ),
     *
     *
     *   @OA\Response(response="201", description="add new area success"),
     *   @OA\Response(response="400", description="parameter error"),
     *   @OA\Response(response="403", description="authorize error"),
     *   @OA\Response(response="404", description="not found error"),
     *   @OA\Response(response="500", description="internal server error"),
     * )
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function add_area(Request $request) {

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:4|max:255',
            'company_id' => 'required|string',
        ]);

        if($validator->fails()){
            return response()->json($this->configFailArray($validator->messages()), 400);
        }

        if (Area::where('name', $request->get('name'))->first() != null) {
            return response()->json($this->configFailArray("same area name already exist."));
        }

        $area = Area::create([
            'name' => $request->get('name'),
            'company_id' => $request->get('company_id')
        ]);

        return response()->json(
            $this->configSuccessArray(
                "new area have added successfully",
                $area->with('company')->find($area->_id)
            )
        );
    }

    /**
     *
     * @OA\Put(path="/admin/area/edit/{_id}",
     *   tags={"Admin Controller"},
     *   summary="Edit area by _id",
     *   description="",
     *   operationId="editArea",
     *   @OA\Parameter(
     *     name="auth_token",
     *     required=true,
     *     in="header",
     *     description="The authorization token",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="_id",
     *     required=true,
     *     in="path",
     *     description="The area Object Id",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="name",
     *     required=true,
     *     in="query",
     *     description="The area name",
     *     @OA\Schema(
     *         type="string",
     *         minLength=4,
     *         maxLength=255
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="company_id",
     *     in="query",
     *     description="company id of area",
     *     @OA\Schema(
     *         type="Object Id",
     *     )
     *   ),
     *
     *
     *   @OA\Response(response="200", description="area info edit success"),
     *   @OA\Response(response="400", description="parameter error"),
     *   @OA\Response(response="403", description="authorize error"),
     *   @OA\Response(response="404", description="not found error"),
     *   @OA\Response(response="500", description="internal server error"),
     * )
     *
     * @param Request $request
     * @param $_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit_area(Request $request, $_id) {
Log::alert($_id);
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:4|max:255',
            'company_id' => 'required|string',
        ]);

        if($validator->fails()){
            return response()->json($this->configFailArray($validator->messages()), 400);
        }

        $area = Area::where('name', $request->get('name'))->first();
        if ($area != null && $area->_id != $_id) {
            return response()->json($this->configFailArray("same area name already exist."));
        }

        $area = Area::find($_id);
        $area->name = $request->get('name');
        $area->company_id = $request->get('company_id');
        $area->save();

        return response()->json(
            $this->configSuccessArray(
                "area have edited successfully",
                $area->with('company')->find($area->_id)
            )
        );
    }

    /**
     *
     * @OA\DELETE(path="/admin/area/delete/{_id}",
     *   tags={"Admin Controller"},
     *   summary="Delete area",
     *   description="",
     *   operationId="deleteArea",
     *   @OA\Parameter(
     *     name="auth_token",
     *     required=true,
     *     in="header",
     *     description="The authorization token",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="_id",
     *     required=true,
     *     in="path",
     *     description="The area Object Id",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *
     *   @OA\Response(response="200", description="area delete success"),
     *   @OA\Response(response="400", description="parameter error"),
     *   @OA\Response(response="403", description="authorize error"),
     *   @OA\Response(response="404", description="not found error"),
     *   @OA\Response(response="500", description="internal server error"),
     * )
     *
     * @param $_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete_area($_id) {

		$data = User::where('company_id', $_id)->first();
		if ($data != null)
			return response()->json($this->configFailArray("This company is already in use."));
		
		$data = Customer::where('company_id', $_id)->first();
		if ($data != null)
			return response()->json($this->configFailArray("This company is already in use."));
		
		$data = Company::where('company_id', $_id)->first();
		if ($data != null)
			return response()->json($this->configFailArray("This company is already in use."));


        Area::destroy($_id);
        return response()->json($this->configSuccessArray("area have deleted successfully."));
    }

    /**
     *
     * @OA\Get(path="/admin/area",
     *   tags={"Admin Controller"},
     *   summary="Get all area list",
     *   description="",
     *   operationId="getArea",
     *   @OA\Parameter(
     *     name="auth_token",
     *     required=true,
     *     in="header",
     *     description="The authorization token",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *
     *   @OA\Response(response="200", description="all area list get successfully"),
     *   @OA\Response(response="400", description="parameter error"),
     *   @OA\Response(response="403", description="authorize error"),
     *   @OA\Response(response="404", description="not found error"),
     *   @OA\Response(response="500", description="internal server error"),
     * )
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_area() {
        return response()->json(
            $this->configSuccessArray(
                "All area list get successfully",
                Area::with('company')->get()
            )
        );
    }

    /**
     *
     * @OA\Get(path="/admin/area/{_id}",
     *   tags={"Admin Controller"},
     *   summary="Get area list by Id",
     *   description="",
     *   operationId="areaInfo",
     *   @OA\Parameter(
     *     name="auth_token",
     *     required=true,
     *     in="header",
     *     description="The authorization token",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="_id",
     *     required=true,
     *     in="path",
     *     description="The Object Id of company that get area info",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Response(response="200", description="get area info by id success"),
     *   @OA\Response(response="400", description="parameter error"),
     *   @OA\Response(response="403", description="authorize error"),
     *   @OA\Response(response="404", description="not found error"),
     *   @OA\Response(response="500", description="internal server error"),
     * )
     *
     *
     * @param $_id : Object Id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_area_byId($_id) {
        return response()->json(
            $this->configSuccessArray(
                "Area info get successfully",
                Area::with('company')->find($_id)
            )
        );
    }

    /**
     *
     * @OA\Get(path="/admin/area/company/{_id}",
     *   tags={"Admin Controller"},
     *   summary="Get area list by CompanyId",
     *   description="",
     *   operationId="areaInfo",
     *   @OA\Parameter(
     *     name="auth_token",
     *     required=true,
     *     in="header",
     *     description="The authorization token",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="_id",
     *     required=true,
     *     in="path",
     *     description="The Object Id of company that get area list",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Response(response="200", description="get area list by company_id success"),
     *   @OA\Response(response="400", description="parameter error"),
     *   @OA\Response(response="403", description="authorize error"),
     *   @OA\Response(response="404", description="not found error"),
     *   @OA\Response(response="500", description="internal server error"),
     * )
     *
     *
     * @param $_id : Object Id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_area_byCompany($_id) {
        return response()->json(
            $this->configSuccessArray(
                "All area list by company id get successfully",
                Area::with('company')->where('company_id', $_id)->get()
            )
        );
    }

}
