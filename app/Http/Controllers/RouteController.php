<?php

namespace App\Http\Controllers;

use App\Http\Models\Customer;
use App\Http\Models\Expenses;
use App\Http\Models\Route;
use App\Http\Models\Schema\CheckList;
use App\Http\Models\Schema\CheckPoint;
use App\Http\Models\Schema\Location;
use App\Http\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class RouteController extends Controller
{

    /**
     *
     * @OA\Post(path="/route/add",
     *   tags={"Route Controller"},
     *   summary="Add route",
     *   description="",
     *   operationId="addRoute",
     *   @OA\Parameter(
     *     name="auth_token",
     *     required=true,
     *     in="header",
     *     description="The authorization token",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="route",
     *     required=true,
     *     in="query",
     *     description="The route info",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *
     *   @OA\Response(response="201", description="add new route success"),
     *   @OA\Response(response="400", description="parameter error"),
     *   @OA\Response(response="403", description="authorize error"),
     *   @OA\Response(response="404", description="not found error"),
     *   @OA\Response(response="500", description="internal server error"),
     * )
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create_route(Request $request) {

        $validator = Validator::make($request->all(), [
            'route' => 'required|string',
        ]);

        if($validator->fails()){
            return response()->json($this->configFailArray($validator->messages()), 400);
        }

        $route = json_decode($request->get("route"), true);

        if (Route::where('name', $route['name'])->first() != null) {
            return response()->json($this->configFailArray("route of same name already is exist"));
        }

        $customer_list = [];
        $array = $route['customer_list'];

        foreach ($array as $value) {

            $checklist = [];
            foreach ($value['checklist'] as $value1) {
                array_push($checklist, new CheckPoint($value1));
            }

            array_push($customer_list,
                new CheckList([
                    'customer_id' => $value['customer_id'],
                    'datetime' => $value['datetime'],
                    'location' => new Location($value['location']),
                    'purpose' => $value['purpose'],
                    'visit_person' => $value['visit_person'],
                    'expenses' => [],
                    'checklist' => $checklist
                ])
            );
        }

        $new_route = Route::create([
            'name' => $route['name'],
            'created_user' => $route['created_user'],
            'assigned_user' => '',
            'expenses' => [],
            'customer_list' => $customer_list,
            'is_approved' => $route['is_approved']
        ]);

        return response()->json(
            $this->configSuccessArray(
                "New route have created successfully",
                $new_route->with('relation1')->with('relation2')->find($new_route->_id)
            ),
            201
        );

    }

    /**
     *
     * @OA\Put(path="/route/edit/{_id}",
     *   tags={"Route Controller"},
     *   summary="Edit route",
     *   description="",
     *   operationId="editRoute",
     *   @OA\Parameter(
     *     name="auth_token",
     *     required=true,
     *     in="header",
     *     description="The authorization token",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="_id",
     *     required=true,
     *     in="path",
     *     description="The Route Object Id",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="name",
     *     required=true,
     *     in="query",
     *     description="The route name",
     *     @OA\Schema(
     *         type="string",
     *         minLength=4,
     *         maxLength=255
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="created_user",
     *     required=true,
     *     in="query",
     *     description="The Object Id of user that created this route",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="assigned_user",
     *     in="query",
     *     description="The Object Id of user that assigned this route",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="is_approved",
     *     required=true,
     *     in="query",
     *     description="Approve status of route",
     *     @OA\Schema(
     *         type="boolean",
     *         default=false
     *     )
     *   ),
     *
     *
     *   @OA\Response(response="200", description="edit route success"),
     *   @OA\Response(response="400", description="parameter error"),
     *   @OA\Response(response="403", description="authorize error"),
     *   @OA\Response(response="404", description="not found error"),
     *   @OA\Response(response="500", description="internal server error"),
     * )
     *
     * @param Request $request
     * @param $_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit_route(Request $request, $_id) {

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:4|max:255',
            'created_user' => 'required|string',
            'assigned_user' => 'string',
            'check_list' => 'required|array',
            'check_list.*' => 'required|string|distinct|min:3',
        ]);

        if($validator->fails()){
            return response()->json($this->configFailArray($validator->messages()), 400);
        }

        $route = Route::find($_id);
        $check = Route::where('name', $request->get('name'))->first();

        if ($check != null && $route->_id != $check->_id) {
            return response()->json($this->configFailArray("route of same name already is exist"));
        }

        $route->name = $request->get('name');
        $route->created_user = $request->get('created_user');
        $route->assigned_user = $request->get('assigned_user');
        $route->check_list = $request->get('check_list');
        $route->is_approved = $request->get('is_approved');
        $route->save();

        return response()->json(
            $this->configSuccessArray(
                "Route edited successfully",
                $route->with('relation1')->with('relation2')->find($route->_id)
            ),
            200
        );
    }

    /**
     *
     * @OA\DELETE(path="/route/delete/{_id}",
     *   tags={"Route Controller"},
     *   summary="Delete Route",
     *   description="",
     *   operationId="deleteRoute",
     *   @OA\Parameter(
     *     name="auth_token",
     *     required=true,
     *     in="header",
     *     description="The authorization token",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="_id",
     *     required=true,
     *     in="path",
     *     description="The Object Id of Route",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *
     *   @OA\Response(response="200", description="delete route success"),
     *   @OA\Response(response="400", description="parameter error"),
     *   @OA\Response(response="403", description="authorize error"),
     *   @OA\Response(response="404", description="not found error"),
     *   @OA\Response(response="500", description="internal server error"),
     * )
     *
     * @param $_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete_route($_id) {

        Route::destroy($_id);
        return response()->json($this->configSuccessArray("Route have deleted successfully."));

    }

    /**
     *
     * @OA\Get(path="/route/get",
     *   tags={"Route Controller"},
     *   summary="Get all route list",
     *   description="",
     *   operationId="getRoute",
     *   @OA\Parameter(
     *     name="auth_token",
     *     required=true,
     *     in="header",
     *     description="The authorization token",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *
     *   @OA\Response(response="200", description="all route list get successfully"),
     *   @OA\Response(response="400", description="parameter error"),
     *   @OA\Response(response="403", description="authorize error"),
     *   @OA\Response(response="404", description="not found error"),
     *   @OA\Response(response="500", description="internal server error"),
     * )
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_route() {
        return response()->json(
            $this->configSuccessArray(
                "All route list get successfully",
                Route::orderBy('created_at', 'desc')->get()
            )
        );
    }

    /**
     *
     * @OA\Get(path="/route/get_by",
     *   tags={"Route Controller"},
     *   summary="Get route list by condition",
     *   description="",
     *   operationId="getRouteCond",
     *   @OA\Parameter(
     *     name="auth_token",
     *     required=true,
     *     in="header",
     *     description="The authorization token",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="search",
     *     in="query",
     *     description="Search string of route name",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="start",
     *     required=true,
     *     in="query",
     *     description="start position that get route list",
     *     @OA\Schema(
     *         type="integer",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="limit",
     *     required=true,
     *     in="query",
     *     description="limit count that get route list",
     *     @OA\Schema(
     *         type="integer",
     *     )
     *   ),
     *
     *   @OA\Response(response="200", description="route list get successfully"),
     *   @OA\Response(response="400", description="parameter error"),
     *   @OA\Response(response="403", description="authorize error"),
     *   @OA\Response(response="404", description="not found error"),
     *   @OA\Response(response="500", description="internal server error"),
     * )
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_route_byCondition(Request $request) {

        $search = $request->get('search');
        $total_count = Route::count();

        $routes = [];
        $user = User::where('auth_token', getallheaders()['auth_token'])->first();

        if ($search == null)  {

            if ($user->role == 'USER') {

                $routes = Route::where('created_user', $user->id)
                    ->orWhere('assigned_user', $user->id)
                    ->skip(intval($request->get('start')))
                    ->take(intval($request->get('limit')))
                    ->orderBy('created_at', 'desc')
                    ->get();

            } else if ($user->role == 'MANAGER') {

                $data = Route::skip(intval($request->get('start')))
                    ->take(intval($request->get('limit')))
                    ->orderBy('created_at', 'desc')
                    ->get();

                foreach ($data as $key => $row) {
                    $customer = Customer::find($row->customer_list[0]->customer_id);
                    if ($customer->area_id == $user->area_id) array_push($routes, $row);
                }

            } else if ($user->role == 'ADMIN') {

                $routes = Route::skip(intval($request->get('start')))
                    ->take(intval($request->get('limit')))
                    ->orderBy('created_at', 'desc')
                    ->get();
            }



        } else {
            $routes = Route::where('name', 'like', "%$search%")
                ->skip(intval($request->get('start')))
                ->take(intval($request->get('limit')))
                ->orderBy('created_at', 'desc')
                ->get();
        }

        return response()->json(
            $this->configSuccessArray(
                "route list get successfully",
                ['total_count' => $total_count, 'routes' => $routes]
            )
        );

    }

    /**
     *
     * @OA\Get(path="/route/get/{_id}",
     *   tags={"Route Controller"},
     *   summary="Get route info by Id",
     *   description="",
     *   operationId="areaInfo",
     *   @OA\Parameter(
     *     name="auth_token",
     *     required=true,
     *     in="header",
     *     description="The authorization token",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="_id",
     *     required=true,
     *     in="path",
     *     description="The Object Id of route",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Response(response="200", description="get route info by Id successfully"),
     *   @OA\Response(response="400", description="parameter error"),
     *   @OA\Response(response="403", description="authorize error"),
     *   @OA\Response(response="404", description="not found error"),
     *   @OA\Response(response="500", description="internal server error"),
     * )
     *
     *
     * @param $_id : Object Id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_route_byId($_id) {
        return response()->json(
            $this->configSuccessArray(
                "Route info get successfully",
                Route::find($_id)
            )
        );
    }

    /**
     *
     * @OA\Put(path="/route/approve/{_id}",
     *   tags={"Route Controller"},
     *   summary="Approve route",
     *   description="",
     *   operationId="approveRoute",
     *   @OA\Parameter(
     *     name="auth_token",
     *     required=true,
     *     in="header",
     *     description="The authorization token",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="_id",
     *     required=true,
     *     in="path",
     *     description="The Object Id of route",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="status",
     *     required=true,
     *     in="query",
     *     description="Approve status",
     *     @OA\Schema(
     *         type="integer",
     *     )
     *   ),
     *   @OA\Response(response="200", description="approve route success"),
     *   @OA\Response(response="400", description="parameter error"),
     *   @OA\Response(response="403", description="authorize error"),
     *   @OA\Response(response="404", description="not found error"),
     *   @OA\Response(response="500", description="internal server error"),
     * )
     *
     *
     * @param Request $request
     * @param $_id : Object Id
     * @return \Illuminate\Http\JsonResponse
     */
    public function approve_route(Request $request, $_id) {

        $validator = Validator::make($request->all(), [
            'status' => 'required|int',
        ]);

        if($validator->fails()){
            return response()->json($this->configFailArray($validator->messages()), 400);
        }

        $route = Route::find($_id);

        if ($route == null) {
            return response()->json($this->configFailArray("Route not exist"));
        }

        if ($request->get('status') == "1") {
        	$route->is_approved = true;
        	$route->assigned_user = "";
        } else {
        	$route->is_approved = false;
        	$route->assigned_user = "rejected";
        }
        $route->save();

        $create_user = $route->with('relation1')->find($route->_id)->relation1;
        $email = $create_user->email;

        $msg = "Admin have approved your request.";
        if (!$route->is_approved) $msg = "Admin have rejected your request.";

        $data = [
            'username' => $create_user->username,
            'route_name' => $route->name,
            'message' => $msg
        ];

        try {
            Mail::send('email.approve', $data, function ($message) use ($email) {
                $message->from(env("MAIL_USERNAME"));
                $message->to($email);
                $message->subject("Approve Route");
            });
        } catch (Exception $exception) {
            Log::info($exception);
        }

        return response()->json($this->configSuccessArray("Route have approved successfully",
            $route->with('relation1')->with('relation2')->find($route->_id)));
    }

    /**
     *
     * @OA\Put(path="/route/assign",
     *   tags={"Route Controller"},
     *   summary="Assign route",
     *   description="",
     *   operationId="assignRoute",
     *   @OA\Parameter(
     *     name="auth_token",
     *     required=true,
     *     in="header",
     *     description="The authorization token",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="route_id",
     *     required=true,
     *     in="query",
     *     description="The Object Id of route",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="user_id",
     *     in="query",
     *     description="The Object Id of route",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *
     *
     *
     *   @OA\Response(response="200", description="assign route success"),
     *   @OA\Response(response="400", description="parameter error"),
     *   @OA\Response(response="403", description="authorize error"),
     *   @OA\Response(response="404", description="not found error"),
     *   @OA\Response(response="500", description="internal server error"),
     * )
     *
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function assigned_route(Request $request) {

        $validator = Validator::make($request->all(), [
            'route_id' => 'required|string',
        ]);

        if($validator->fails()){
            return response()->json($this->configFailArray($validator->messages()), 400);
        }

        $user_id ="";
        if ($request->get('user_id') != null) $user_id = $request->get('user_id');

        $route = Route::find($request->get('route_id'));
        $route->assigned_user = $user_id;
        $route->save();

        return response()->json(
            $this->configSuccessArray(
                "The route has been assigned to that user.",
                $route->with('relation1')->with('relation2')->find($route->_id)
            ),
            200
        );

    }


    /**
     *
     * @OA\Put(path="/route/update-expense",
     *   tags={"Route Controller"},
     *   summary="Assign route",
     *   description="",
     *   operationId="assignRoute",
     *   @OA\Parameter(
     *     name="auth_token",
     *     required=true,
     *     in="header",
     *     description="The authorization token",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="route_id",
     *     required=true,
     *     in="query",
     *     description="The Object Id of route",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="is_update",
     *     required=true,
     *     in="query",
     *     description="The update status",
     *     @OA\Schema(
     *         type="string",
     *         enum={"true", "false"}
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="expenses",
     *     required=true,
     *     in="query",
     *     description="The JSON String for expense data",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *
     *
     *   @OA\Response(response="200", description="assign route success"),
     *   @OA\Response(response="400", description="parameter error"),
     *   @OA\Response(response="403", description="authorize error"),
     *   @OA\Response(response="404", description="not found error"),
     *   @OA\Response(response="500", description="internal server error"),
     * )
     *
     *
     * When add or update expense in Route, update expense id array and add or update expense data
     * When update expense in checklist, update expense data
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update_expense(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'route_id' => 'required|string',
            'is_update' => 'required',
            'expenses' => 'required|string'
        ]);

        if ($validator->fails()) {
            return response()->json($this->configFailArray($validator->messages()), 400);
        }

        if ($request->hasFile('file') && $request->file('file')->isValid()) {

            $image_storage = config("filesystems.disks.upload_storage");
            $image_name = $request->file("file")->getClientOriginalName();

            if (!file_exists($image_storage)) mkdir($image_storage, 0777, true);

            $request->file('file')->move($image_storage, $image_name);
        }

        $data = json_decode($request->get("expenses"), true);
        $route = Route::find($request->get('route_id'));

        if ($request->get('is_update') == "true") {
            $expense = Expenses::find($request->get('expense_id'));
            $expense->date = $data['date'];
            $expense->time = $data['time'];
            $expense->title = $data['title'];
            $expense->amount = $data['amount'];
            $expense->details = $data['details'];
            $expense->billable = $data['billable'];
            $expense->attachment = $data['attachment'];
            $expense->save();
        } else {
            $expense = Expenses::create($data);
            $expenses = $route->expenses;
            array_push($expenses, $expense->_id);
            $route->expenses = $expenses;
            $route->save();
        }

        $msg = "Expense added successfully";
        if ($request->get('is_update')) $msg = "Expense updated successfully";

        return response()->json($this->configSuccessArray($msg, ['route' => $route, 'expense' => $expense]));
    }



    /**
     *
     * @OA\Put(path="/route/update-customer",
     *   tags={"Route Controller"},
     *   summary="Assign route",
     *   description="",
     *   operationId="updateCustomer",
     *   @OA\Parameter(
     *     name="auth_token",
     *     required=true,
     *     in="header",
     *     description="The authorization token",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="route_id",
     *     required=true,
     *     in="query",
     *     description="The Object Id of route",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="expenses",
     *     required=true,
     *     in="query",
     *     description="The JSON String for expense data",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *
     *   @OA\Parameter(
     *     name="customer_list",
     *     required=true,
     *     in="query",
     *     description="The JSON String for customer list data",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *
     *   @OA\Parameter(
     *     name="customer_index",
     *     required=true,
     *     in="query",
     *     description="Customer id to update",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *
     *   @OA\Response(response="200", description="assign route success"),
     *   @OA\Response(response="400", description="parameter error"),
     *   @OA\Response(response="403", description="authorize error"),
     *   @OA\Response(response="404", description="not found error"),
     *   @OA\Response(response="500", description="internal server error"),
     * )
     *
     *
     * When add expenses in checklist,
     * update customer list and add expense data
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update_customer(Request $request) {

        $validator = Validator::make($request->all(), [
            'route_id' => 'required|string',
            'customer_list' => 'required|string',
            'expenses' => 'required|string'
        ]);

        if ($validator->fails()) {
            return response()->json($this->configFailArray($validator->messages()), 400);
        }

        if ($request->hasFile('file') && $request->file('file')->isValid()) {

            $image_storage = config("filesystems.disks.upload_storage");
            $image_name = $request->file("file")->getClientOriginalName();

            if (!file_exists($image_storage)) mkdir($image_storage, 0777, true);

            $request->file('file')->move($image_storage, $image_name);
        }

        $customer_list = json_decode($request->get("customer_list"), true);
        $result = [];

        $route = Route::find($request->get('route_id'));
        $expense = Expenses::create(json_decode($request->get('expenses'), true));
        $customer_index = $request->get('customer_index');
        foreach ($customer_list as $key=>$row) {

            $checklist = [];
            foreach ($row['checklist'] as $item) {
                array_push($checklist, new CheckPoint($item));
            }

            $expenses = $row['expenses'];
            if ($key == $customer_index) {
                array_push($expenses, $expense->_id);
            }

            $customer_data = new CheckList([
                'customer_id' => $row['customer_id'],
                'datetime' => $row['datetime'],
                'location' => new Location($row['location']),
                'purpose' => $row['purpose'],
                'visit_person' => $row['visit_person'],
                'expenses' => $expenses,
                'checklist' => $checklist,
            ]);

            array_push($result, $customer_data);
        }

        $route->customer_list = $result;
        $route->save();

        $msg = "Expense added successfully";
        if ($request->get('is_update')) $msg = "Expense updated successfully";

        return response()->json($this->configSuccessArray($msg, ['route' => $route, 'expense' => $expense]));

    }



    /**
     *
     * @OA\Put(path="/route/update-checklist",
     *   tags={"Route Controller"},
     *   summary="Assign route",
     *   description="",
     *   operationId="assignRoute",
     *   @OA\Parameter(
     *     name="auth_token",
     *     required=true,
     *     in="header",
     *     description="The authorization token",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="route_id",
     *     required=true,
     *     in="query",
     *     description="The Object Id of route",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="customer_list",
     *     required=true,
     *     in="query",
     *     description="The JSON String for customer list data",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *
     *   @OA\Response(response="200", description="assign route success"),
     *   @OA\Response(response="400", description="parameter error"),
     *   @OA\Response(response="403", description="authorize error"),
     *   @OA\Response(response="404", description="not found error"),
     *   @OA\Response(response="500", description="internal server error"),
     * )
     *
     *
     * Update checklist status
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update_checklist(Request $request) {

        $validator = Validator::make($request->all(), [
            'route_id' => 'required|string',
            'customer_list' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json($this->configFailArray($validator->messages()), 400);
        }

        $customer_list = json_decode($request->get("customer_list"), true);
        $result = [];

        foreach ($customer_list as $row) {

            $checklist = [];
            foreach ($row['checklist'] as $item) {
                array_push($checklist, new CheckPoint($item));
            }

            $customer_data = new CheckList([
                'customer_id' => $row['customer_id'],
                'datetime' => $row['datetime'],
                'location' => new Location($row['location']),
                'purpose' => $row['purpose'],
                'visit_person' => $row['visit_person'],
                'expenses' => $row['expenses'],
                'checklist' => $checklist,
            ]);

            array_push($result, $customer_data);
        }

        $route = Route::find($request->get('route_id'));
        $route->customer_list = $result;
        $route->save();

        return response()->json($this->configSuccessArray("Checklist allow successfully", $route));
    }



    /**
     *
     * @OA\Put(path="/route/update-checklist-route",
     *   tags={"Route Controller"},
     *   summary="Assign route",
     *   description="",
     *   operationId="assignRoute",
     *   @OA\Parameter(
     *     name="auth_token",
     *     required=true,
     *     in="header",
     *     description="The authorization token",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="route_id",
     *     required=true,
     *     in="query",
     *     description="The Object Id of route",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="check_list",
     *     required=true,
     *     in="query",
     *     description="The JSON String for route check list data",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *
     *   @OA\Response(response="200", description="assign route success"),
     *   @OA\Response(response="400", description="parameter error"),
     *   @OA\Response(response="403", description="authorize error"),
     *   @OA\Response(response="404", description="not found error"),
     *   @OA\Response(response="500", description="internal server error"),
     * )
     *
     *
     * Update checklist status
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update_checklist_route(Request $request) {

        $validator = Validator::make($request->all(), [
            'route_id' => 'required|string',
            'check_list' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json($this->configFailArray($validator->messages()), 400);
        }

        $check_list = json_decode($request->get("check_list"), true);

        $route = Route::find($request->get('route_id'));
        $dataList = [];
        foreach ($check_list as $row) {
            array_push($dataList, new CheckPoint($row));
        }
        $route->checklist = $dataList;
        $route->save();

        return response()->json($this->configSuccessArray("Checklist allow successfully", $route));
    }


    
    /**
     *
     * @OA\Put(path="/route/delete-customer-expense",
     *   tags={"Route Controller"},
     *   summary="Assign route",
     *   description="",
     *   operationId="deleteCustomerExpense",
     *   @OA\Parameter(
     *     name="auth_token",
     *     required=true,
     *     in="header",
     *     description="The authorization token",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="route_id",
     *     required=true,
     *     in="query",
     *     description="The Object Id of route",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="customer_list",
     *     required=true,
     *     in="query",
     *     description="The List of customer",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="expense_id",
     *     required=true,
     *     in="query",
     *     description="The Object Id of expense",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *
     *   @OA\Response(response="200", description="assign route success"),
     *   @OA\Response(response="400", description="parameter error"),
     *   @OA\Response(response="403", description="authorize error"),
     *   @OA\Response(response="404", description="not found error"),
     *   @OA\Response(response="500", description="internal server error"),
     * )
     *
     *
     * Update checklist status
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete_customer_expense(Request $request) {

        $validator = Validator::make($request->all(), [
            'route_id' => 'required|string',
            'customer_list' => 'required|string',
            'expense_id' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json($this->configFailArray($validator->messages()), 400);
        }

        $customer_list = json_decode($request->get("customer_list"), true);
        $result = [];

        foreach ($customer_list as $row) {

            $checklist = [];
            foreach ($row['checklist'] as $item) {
                array_push($checklist, new CheckPoint($item));
            }

            $customer_data = new CheckList([
                'customer_id' => $row['customer_id'],
                'datetime' => $row['datetime'],
                'location' => new Location($row['location']),
                'purpose' => $row['purpose'],
                'visit_person' => $row['visit_person'],
                'expenses' => $row['expenses'],
                'checklist' => $checklist,
            ]);

            array_push($result, $customer_data);
        }

        $route = Route::find($request->get('route_id'));
        $route->customer_list = $result;
        $route->save();

        Expenses::destroy($request->get("expense_id"));

        return response()->json($this->configSuccessArray("Checklist allow successfully", $route));
    }

    
    /**
     *
     * @OA\Put(path="/route/delete-route-expense",
     *   tags={"Route Controller"},
     *   summary="Assign route",
     *   description="",
     *   operationId="deleteRouteExpense",
     *   @OA\Parameter(
     *     name="auth_token",
     *     required=true,
     *     in="header",
     *     description="The authorization token",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="route_id",
     *     required=true,
     *     in="query",
     *     description="The Object Index of route",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="expense_index",
     *     required=true,
     *     in="query",
     *     description="The Object Index of expense",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *
     *   @OA\Response(response="200", description="assign route success"),
     *   @OA\Response(response="400", description="parameter error"),
     *   @OA\Response(response="403", description="authorize error"),
     *   @OA\Response(response="404", description="not found error"),
     *   @OA\Response(response="500", description="internal server error"),
     * )
     *
     *
     * Update checklist status
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete_route_expense(Request $request) {

        $validator = Validator::make($request->all(), [
            'route_id' => 'required|string',
            'expense_index' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($this->configFailArray($validator->messages()), 400);
        }

        $expense_index = $request->get("expense_index");
        $route = Route::find($request->get('route_id'));
        $expense_list = [];
        foreach($route->expenses as $index=>$expense) {
            if ($index == $expense_index) {
                Log::error("DELETE: " . $expense);
                Expenses::destroy($expense);
            } else {
                array_push($expense_list, $expense);
            }
        }
        $route->expenses = $expense_list;
        $route->save();

        return response()->json($this->configSuccessArray("Checklist allow successfully", $route));
    }


    
    /**
     *
     * @OA\Put(path="/route/delete-route-customer",
     *   tags={"Route Controller"},
     *   summary="Assign route",
     *   description="",
     *   operationId="deleteRouteCustomer",
     *   @OA\Parameter(
     *     name="auth_token",
     *     required=true,
     *     in="header",
     *     description="The authorization token",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="route_id",
     *     required=true,
     *     in="query",
     *     description="The Object Id of route",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="customer_list",
     *     required=true,
     *     in="query",
     *     description="The List of customer",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *
     *   @OA\Response(response="200", description="assign route success"),
     *   @OA\Response(response="400", description="parameter error"),
     *   @OA\Response(response="403", description="authorize error"),
     *   @OA\Response(response="404", description="not found error"),
     *   @OA\Response(response="500", description="internal server error"),
     * )
     *
     *
     * Update checklist status
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete_route_customer(Request $request) {

        $validator = Validator::make($request->all(), [
            'route_id' => 'required|string',
            'customer_list' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json($this->configFailArray($validator->messages()), 400);
        }

        $customer_list = json_decode($request->get("customer_list"), true);
        $result = [];

        foreach ($customer_list as $row) {

            $checklist = [];
            foreach ($row['checklist'] as $item) {
                array_push($checklist, new CheckPoint($item));
            }

            $customer_data = new CheckList([
                'customer_id' => $row['customer_id'],
                'datetime' => $row['datetime'],
                'location' => new Location($row['location']),
                'purpose' => $row['purpose'],
                'visit_person' => $row['visit_person'],
                'expenses' => $row['expenses'],
                'checklist' => $checklist,
            ]);

            array_push($result, $customer_data);
        }

        $route = Route::find($request->get('route_id'));
        $route->customer_list = $result;
        $route->save();

        return response()->json($this->configSuccessArray("Checklist allow successfully", $route));
    }
}
