<?php

namespace App\Http\Controllers;

use App\Http\Models\Area;
use App\Http\Models\Customer;
use App\Http\Models\Expenses;
use App\Http\Models\Route;
use App\Http\Models\Schema\Location;
use App\Http\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class CustomerController extends Controller
{

    /**
     * @OA\Post(path="/customer/add",
     *   tags={"Customer Controller"},
     *   summary="Add new customer",
     *   description="",
     *   operationId="addCustomer",
     *   @OA\Parameter(
     *     name="auth_token",
     *     required=true,
     *     in="header",
     *     description="The authorization token",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="name",
     *     required=true,
     *     in="query",
     *     description="The customer name",
     *     @OA\Schema(
     *         type="string",
     *         minLength=6,
     *         maxLength=20
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="email",
     *     required=true,
     *     in="query",
     *     description="The customer email",
     *     @OA\Schema(
     *         type="string",
     *         format="email"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="phone",
     *     required=true,
     *     in="query",
     *     description="The customer phone number",
     *     @OA\Schema(
     *         type="string",
     *         minLength=10,
     *         maxLength=20,
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="address[]",
     *     in="query",
     *     required=true,
     *     description="address list of customer",
     *     @OA\Schema(
     *         type="array",
     *         @OA\Items(type="string"),
     *     ),
     *     style="form",
     *     explode=true
     *   ),
     *   @OA\Parameter(
     *     name="location[]",
     *     in="query",
     *     description="location list of customer",
     *     required=true,
     *     @OA\Schema(
     *        type="array",
     *        @OA\Items(ref="#/components/schemas/Location"),
     *     ),
     *     style="form"
     *   ),
     *   @OA\Parameter(
     *     name="default_address",
     *     in="query",
     *     description="default address index of customer",
     *     @OA\Schema(
     *         type="integer"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="products[]",
     *     in="query",
     *     required=true,
     *     description="products that the customer is interested in",
     *     @OA\Schema(
     *         type="array",
     *         @OA\Items(type="string"),
     *     ),
     *     style="form",
     *     explode=true
     *   ),
     *   @OA\Parameter(
     *     name="services[]",
     *     in="query",
     *     required=true,
     *     description="services that the customer is interested in",
     *     @OA\Schema(
     *         type="array",
     *         @OA\Items(type="string"),
     *     ),
     *     style="form",
     *     explode=true
     *   ),
     *   @OA\Parameter(
     *     name="area_id",
     *     in="query",
     *     description="area id of user",
     *     @OA\Schema(
     *         type="Object Id",
     *     )
     *   ),
     *   @OA\Response(response="201", description="new customer add success"),
     *   @OA\Response(response="400", description="parameter error"),
     *   @OA\Response(response="404", description="not found error"),
     *   @OA\Response(response="500", description="internal server error"),
     * )
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function add_customer(Request $request) {

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:20|min:6',
            'email' => 'required|string|email|max:255',
            'phone' => 'required|string|min:10|max:20',
            'address' => 'required|array',
            'location' => 'required|array',
            'products' => 'required|array',
            'services' => 'required|array',
            'area_id' => 'required|string'
        ]);

        if($validator->fails()){
            return response()->json($this->configFailArray($validator->messages()), 400);
        }

        if (Customer::where('name', $request->get('name'))->first() != null) {
            return response()->json($this->configFailArray("same customer name already is exist."));
        }

        if (Customer::where('email', $request->get('email'))->first() != null) {
            return response()->json($this->configFailArray("same customer email already is exist."));
        }

        if (Customer::where('phone', $request->get('phone'))->first() != null) {
            return response()->json($this->configFailArray("same customer phone number already is exist."));
        }

        $location = [];
        $data = $request->get('location');

        foreach ($data as $key => $row) {
            $latlng = explode(", ", $row);
            array_push($location,
                new Location([
                   'latitude' => doubleval(trim($latlng[0])),
                   'longitude' => doubleval(trim($latlng[1]))
                ]));
        }

        $customer = Customer::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'phone' => $request->get('phone'),
            'address' => $request->get('address'),
            'location' => $location,
            'default_address' => intval($request->get('default_address')),
            'products' => $request->get('products'),
            'services' => $request->get('services'),
            'company_id' => Area::find($request->get('area_id'))->company_id,
            'area_id' => $request->get('area_id')
        ]);

        return response()->json(
            $this->configSuccessArray(
                "new customer have created successfully",
                $customer->with('company')->with('area')->find($customer->_id)),
            201
        );
    }

    /**
     * @OA\Put(path="/customer/edit/{_id}",
     *   tags={"Customer Controller"},
     *   summary="Edit customer",
     *   description="",
     *   operationId="editCustomer",
     *   @OA\Parameter(
     *     name="auth_token",
     *     required=true,
     *     in="header",
     *     description="The authorization token",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="_id",
     *     required=true,
     *     in="path",
     *     description="The customer Object Id",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="name",
     *     required=true,
     *     in="query",
     *     description="The customer name",
     *     @OA\Schema(
     *         type="string",
     *         minLength=6,
     *         maxLength=20
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="email",
     *     required=true,
     *     in="query",
     *     description="The customer email",
     *     @OA\Schema(
     *         type="string",
     *         format="email"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="phone",
     *     required=true,
     *     in="query",
     *     description="The customer phone number",
     *     @OA\Schema(
     *         type="string",
     *         minLength=10,
     *         maxLength=20,
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="address[]",
     *     in="query",
     *     required=true,
     *     description="address list of customer",
     *     @OA\Schema(
     *         type="array",
     *         @OA\Items(type="string"),
     *     ),
     *     style="form",
     *     explode=true
     *   ),
     *   @OA\Parameter(
     *     name="location[]",
     *     in="query",
     *     description="location list of customer",
     *     required=true,
     *     @OA\Schema(
     *        type="array",
     *        @OA\Items(ref="#/components/schemas/Location"),
     *     ),
     *     style="form"
     *   ),
     *   @OA\Parameter(
     *     name="default_address",
     *     in="query",
     *     description="default address index of customer",
     *     @OA\Schema(
     *         type="integer"
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="products[]",
     *     in="query",
     *     required=true,
     *     description="products that the customer is interested in",
     *     @OA\Schema(
     *         type="array",
     *         @OA\Items(type="string"),
     *     ),
     *     style="form",
     *     explode=true
     *   ),
     *   @OA\Parameter(
     *     name="services[]",
     *     in="query",
     *     required=true,
     *     description="services that the customer is interested in",
     *     @OA\Schema(
     *         type="array",
     *         @OA\Items(type="string"),
     *     ),
     *     style="form",
     *     explode=true
     *   ),
     *   @OA\Parameter(
     *     name="area_id",
     *     in="query",
     *     description="area id of user",
     *     @OA\Schema(
     *         type="Object Id",
     *     )
     *   ),
     *   @OA\Response(response="200", description="customer edit success"),
     *   @OA\Response(response="400", description="parameter error"),
     *   @OA\Response(response="404", description="not found error"),
     *   @OA\Response(response="500", description="internal server error"),
     * )
     * @param Request $request
     * @param $_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit_customer(Request $request, $_id) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:20|min:6',
            'email' => 'required|string|email|max:255',
            'phone' => 'required|string|min:10|max:20',
            'address' => 'required|array',
            'location' => 'required|array',
            'products' => 'required|array',
            'services' => 'required|array',
            'area_id' => 'required|string'
        ]);

        if($validator->fails()){
            return response()->json($this->configFailArray($validator->messages()), 400);
        }

        $customer = Customer::find($_id);

        $check = Customer::where('name', $request->get('name'))->first();
        if ($check != null && $check->_id != $customer->_id) {
            return response()->json($this->configFailArray("same customer name already is exist."));
        }

        $check = Customer::where('email', $request->get('email'))->first();
        if ($check != null && $check->_id != $customer->_id) {
            return response()->json($this->configFailArray("same customer email already is exist."));
        }

        $check = Customer::where('phone', $request->get('phone'))->first();
        if ($check != null && $check->_id != $customer->_id) {
            return response()->json($this->configFailArray("same customer phone number already is exist."));
        }

        $location = [];
        $data = $request->get('location');

        foreach ($data as $key => $row) {
            $latlng = explode(", ", $row);
            array_push($location,
                new Location([
                    'latitude' => doubleval(trim($latlng[0])),
                    'longitude' => doubleval(trim($latlng[1]))
                ]));
        }

        $customer->name = $request->get('name');
        $customer->email = $request->get('email');
        $customer->phone = $request->get('phone');
        $customer->address = $request->get('address');
        $customer->location = $location;
        $customer->default_address =$request->get('default_address');
        $customer->products = $request->get('products');
        $customer->services = $request->get('services');
        $customer->company_id = Area::find($request->get('area_id'))->company_id;
        $customer->area_id = $request->get('area_id');
        $customer->save();

        return response()->json(
            $this->configSuccessArray(
                "customer have edited successfully",
                $customer->with('company')->with('area')->find($customer->_id)),
            201
        );
    }

    /**
     *
     * @OA\DELETE(path="/customer/delete/{_id}",
     *   tags={"Customer Controller"},
     *   summary="Delete area",
     *   description="",
     *   operationId="deleteArea",
     *   @OA\Parameter(
     *     name="auth_token",
     *     required=true,
     *     in="header",
     *     description="The authorization token",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="_id",
     *     required=true,
     *     in="path",
     *     description="The customer Object Id",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *
     *   @OA\Response(response="200", description="customer delete success"),
     *   @OA\Response(response="400", description="parameter error"),
     *   @OA\Response(response="403", description="authorize error"),
     *   @OA\Response(response="404", description="not found error"),
     *   @OA\Response(response="500", description="internal server error"),
     * )
     *
     * @param $_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete_customer($_id) {
    	
    	$route = Route::all();
    	foreach($route as $row) {
    		$customer_list = $row->customer_list;
    		foreach($customer_list as $row1) {
    			if ($row1->customer_id == $_id) {
    				return response()->json($this->configFailArray("This customer is already in use."));
    			}
    		}
    	}
    	
        Customer::destroy($_id);
        return response()->json($this->configSuccessArray("customer have deleted successfully."));
    }

    /**
     *
     * @OA\Get(path="/customer/get",
     *   tags={"Customer Controller"},
     *   summary="Get all customer list",
     *   description="",
     *   operationId="getCustomer",
     *   @OA\Parameter(
     *     name="auth_token",
     *     required=true,
     *     in="header",
     *     description="The authorization token",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *
     *   @OA\Response(response="200", description="all customer list get successfully"),
     *   @OA\Response(response="400", description="parameter error"),
     *   @OA\Response(response="403", description="authorize error"),
     *   @OA\Response(response="404", description="not found error"),
     *   @OA\Response(response="500", description="internal server error"),
     * )
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_customer() {
    	
    	$user = User::where('auth_token', getallheaders()['auth_token'])->first();
    	
    	if ($user->role == 'ADMIN') {
    		return response()->json(
	            $this->configSuccessArray(
	                "All customer list get successfully",
	                Customer::orderBy('created_at', 'desc')->with('company')->with('area')->get()
		        )
	        );
    	} else {
    		return response()->json(
	            $this->configSuccessArray(
	                "All customer list get successfully!!!",
	                Customer::orderBy('created_at', 'desc')->with('company')->with('area')
	        			->where('area_id', $user->area_id)->get()
		        )
	        );
    	}
    	
        
    }

    /**
     *
     * @OA\Get(path="/customer/get/{_id}",
     *   tags={"Customer Controller"},
     *   summary="Get customer info by Id",
     *   description="",
     *   operationId="getCustomerInfo",
     *   @OA\Parameter(
     *     name="auth_token",
     *     required=true,
     *     in="header",
     *     description="The authorization token",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="_id",
     *     required=true,
     *     in="path",
     *     description="The Object Id of customer",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *
     *   @OA\Response(response="200", description="customer info get successfully"),
     *   @OA\Response(response="400", description="parameter error"),
     *   @OA\Response(response="403", description="authorize error"),
     *   @OA\Response(response="404", description="not found error"),
     *   @OA\Response(response="500", description="internal server error"),
     * )
     *
     * @param $_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_customer_byId($_id) {
        return response()->json(
            $this->configSuccessArray(
                "Customer info get successfully",
                Customer::with('company')->with('area')->find($_id)
            )
        );
    }

    /**
     *
     * @OA\Get(path="/customer/get_by",
     *   tags={"Customer Controller"},
     *   summary="Get customer list by id array",
     *   description="",
     *   operationId="getCustomerInfo",
     *   @OA\Parameter(
     *     name="auth_token",
     *     required=true,
     *     in="header",
     *     description="The authorization token",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="search",
     *     in="query",
     *     description="Search string of customer name",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="start",
     *     required=true,
     *     in="query",
     *     description="start position that get customer list",
     *     @OA\Schema(
     *         type="integer",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="limit",
     *     required=true,
     *     in="query",
     *     description="limit count that get customer list",
     *     @OA\Schema(
     *         type="integer",
     *     )
     *   ),
     *
     *   @OA\Response(response="200", description="customer list get successfully"),
     *   @OA\Response(response="400", description="parameter error"),
     *   @OA\Response(response="403", description="authorize error"),
     *   @OA\Response(response="404", description="not found error"),
     *   @OA\Response(response="500", description="internal server error"),
     * )
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_customer_byCondition(Request $request) {

        $search = $request->get('search');
        $total_count = Customer::count();

        $user = User::where('auth_token', getallheaders()['auth_token'])->first();

        if ($search == null)  {

            if ($user->role == 'ADMIN') {

                $customer = Customer::with('company')->with('area')
                    ->skip(intval($request->get('start')))
                    ->take(intval($request->get('limit')))
                    ->orderBy('created_at', 'desc')
                    ->get();

            } else {

                $customer = Customer::with('company')->with('area')
                    ->where('area_id', $user->area_id)
                    ->skip(intval($request->get('start')))
                    ->take(intval($request->get('limit')))
                    ->orderBy('created_at', 'desc')
                    ->get();

            }

        } else {
            $customer = Customer::with('company')->with('area')
            	->where('area_id', $user->area_id)
                ->where('name', 'like', "%$search%")
                ->orwhere('email', 'like', "%$search%")
                ->skip(intval($request->get('start')))
                ->take(intval($request->get('limit')))
                ->orderBy('created_at', 'desc')
                ->get();
        }

        return response()->json(
            $this->configSuccessArray(
                "customer list get successfully",
                ['total_count' => $total_count, 'customers' => $customer]
            )
        );

    }

    /**
     *
     * @OA\Get(path="/customer/get_array",
     *   tags={"Customer Controller"},
     *   summary="Get customer info by Array",
     *   description="",
     *   operationId="getCustomerArray",
     *   @OA\Parameter(
     *     name="auth_token",
     *     required=true,
     *     in="header",
     *     description="The authorization token",
     *     @OA\Schema(
     *         type="string",
     *     )
     *   ),
     *   @OA\Parameter(
     *     name="id_array[]",
     *     required=true,
     *     in="query",
     *     description="The Object Id Array of customer",
     *     @OA\Schema(
     *         type="array",
     *         @OA\Items(type="string"),
     *     ),
     *   ),
     *   @OA\Parameter(
     *     name="expenses[]",
     *     required=true,
     *     in="query",
     *     description="The Object Id Array of expenses",
     *     @OA\Schema(
     *         type="array",
     *         @OA\Items(type="string"),
     *     ),
     *   ),
     *
     *   @OA\Response(response="200", description="customer list get successfully"),
     *   @OA\Response(response="400", description="parameter error"),
     *   @OA\Response(response="403", description="authorize error"),
     *   @OA\Response(response="404", description="not found error"),
     *   @OA\Response(response="500", description="internal server error"),
     * )
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_customer_byArray(Request $request) {

        $customers = [];
        if ($request->get('id_array') != null) {
	        foreach ($request->get('id_array') as $_id) {
	            $customer = Customer::with('company')->with('area')->find($_id);
	            array_push($customers, $customer);
	        }
        }

        $expenses = [];
        if ($request->get('expenses') != null) {
            foreach ($request->get('expenses') as $_id) {
                $expense = Expenses::find($_id);
                array_push($expenses, $expense);
            }
        }

        return response()->json(
            $this->configSuccessArray(
                "customer list get successfully",
                [
                    'customer' => $customers,
                    'expenses' => $expenses
                ]
            )
        );
    }

}
