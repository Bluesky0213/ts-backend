<!DOCTYPE html>
<html lang="en">
<head>
    <title>Travelling Salesman</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?=asset('css/bootstrap.min.css');?>">
    <script src="<?=asset('js/bootstrap.min.js');?>"></script>
    <link rel="shortcut icon" href="<?=asset('favicon.ico');?>" />
</head>
<body class="container">

<div class="text-center" style="margin: 50px; font-size: 40px; font-weight: bold">
    <img src="<?=asset('/app_logo.png');?>" style="margin-right: 20px">
</div>

<div class="container">
    <div class="row">
        <div class="col-lg-offset-3 col-lg-6 col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6 col-xs-12">
            <div class="jumbotron" style="font-size: 15px">
                <div class="text-center" style="margin-bottom: 20px">
                    <h3>Welcome to Travelling Salesman</h3>
                </div>
                <br> Hi, {{$username}} <br>
                Thanks so much for joining us system.<br>
                You can use our travelling salesman.<br>
                Your role is "{{$role}}" in our app.<br>
                default password : 123456 <br>
                Please change password in mobile app.
            </div>
        </div>
    </div>
</div>

</body>
</html>